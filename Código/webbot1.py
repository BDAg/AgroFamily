import telebot
from bs4 import BeautifulSoup as bs
import requests

CHAVE_API = "6512774547:AAHbtmePCNkCsZcXN2k2AZoilPgAuyUxUaU"

bot = telebot.TeleBot(CHAVE_API)

def extrair_noticias():
    url = 'https://summitagro.estadao.com.br/sustentabilidade/'
    site = requests.get(url)
    conteudo = site.text
    soup = bs(conteudo, 'html.parser')

    link_noticias = []

    for i in range(10):
        quero = soup.find_all('div', class_='image-content-post')
        link_noticias.append(quero[i].parent.get('href'))

    noticias = []

    for i in range(10):
        site_noticias = link_noticias[i]
        site_baixado = requests.get(site_noticias)
        conteudo = site_baixado.text
        sopinha = bs(conteudo, 'html.parser')

        titulo = sopinha.find('div', class_='header-blog').find('h1').text
    
        paragrafos = []
        subtitulo = sopinha.find_all('h2', class_='wp-block-heading')
        for paragrafo in sopinha.find_all('p'):
            texto_paragrafo = paragrafo.get_text(strip=True)
            if texto_paragrafo and not any(indesejado in texto_paragrafo for indesejado in ["Leia também", "O maior", "Estadão Blue", "cookie-check", "Plano Safra incentiva uso de energia elétrica na fazenda", "Contato", "summit@estadao.com", "(Fonte: GettyImages/Reprodução)", "Leia também:", "Inscreva-se gratuitamente e participe do maior evento de agronegócio do Brasil, que acontecerá em 26 de outubro."]):
                paragrafos.append(texto_paragrafo)
            if subtitulo in sopinha.find_all('h2', class_='wp-block-heading'):
                paragrafos.append(subtitulo)
        noticias.append({'titulo': titulo, 'paragrafos': paragrafos})

    return noticias

@bot.message_handler(commands=["Noticia"])
def Noticia(mensagem):
    noticias = extrair_noticias()
    
    for noticia in noticias:
        bot.send_message(mensagem.chat.id, f"<b>{noticia['titulo']}</b>", parse_mode='HTML')
        for paragrafo in noticia['paragrafos']:
            bot.send_message(mensagem.chat.id, paragrafo)

def verificar(mensagem):
    return True 

@bot.message_handler(func=verificar)
def responder(mensagem):
    texto = """
    Clique no item abaixo para receber noticias sobre a agricultura sustentável:
    /Noticia
    """
    bot.reply_to(mensagem, texto)

bot.polling()
